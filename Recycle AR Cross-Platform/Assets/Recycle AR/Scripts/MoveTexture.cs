﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTexture : MonoBehaviour
{
    [Header("Settings move texture")]
    [Space(10)]
    public int NumMaterial;
    [Range(-1f,1f)]
    public float speed = 0.2f;
    

    private Material[] mat;
    private Vector2 offset;
    private float i = 0;
    
    /// <summary>
    /// get component materials in this gameobject
    /// </summary>
    void Start()
    {
        mat = gameObject.GetComponent<Renderer>().materials;
    }

    /// <summary>
    /// move offset texture 
    /// u can use direction 
    /// right: speed = -1
    /// left: speed = 1
    /// </summary>
    void Update()
    {
        i += Time.deltaTime*speed;
        offset = new Vector2(i,0f);
        mat[NumMaterial].mainTextureOffset = offset;
    }
}
