﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Apple.ReplayKit;
using UnityEngine.UI;

public class RecorderUnity : MonoBehaviour
{
    public Image statusReco;
    private int i;

    public void OnRecording()
    {
        i += 1;
        if (i == 1)
        {
            if (!ReplayKit.isRecording)
            {
                ReplayKit.StartRecording(true, true);
                statusReco.color = Color.red;
            }
        }

        if (i > 1)
        {
            if (ReplayKit.isRecording)
            {
                statusReco.color = Color.white;
                ReplayKit.StopRecording();
            }
            StartCoroutine(OpenPreview());
            
            i = 0;
        }      
        
    }

    IEnumerator OpenPreview()
    {
        yield return new WaitForSeconds(1.5f);
        if (ReplayKit.recordingAvailable)
        {
            ReplayKit.Preview();
        }
    }

    /*
    public void OnStopRecording()
    {
        if (ReplayKit.isRecording)
        {
            statusReco.color = Color.white;
            ReplayKit.StopRecording();            
        }

        if (ReplayKit.recordingAvailable)
        {
            ReplayKit.Preview();
        }
    }

    public void OnOpenRecording()
    {
        if (ReplayKit.recordingAvailable)
        {
            ReplayKit.Preview();
        }
    }*/
}
