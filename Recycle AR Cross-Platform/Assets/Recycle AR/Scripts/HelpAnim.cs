﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpAnim : MonoBehaviour
{
    public GameObject HelpPanel;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartHelp());
    }

    IEnumerator StartHelp()
    {
       /* if (PlayerPrefs.GetInt("Help") == 0)
        {*/
            HelpPanel.GetComponent<Animator>().enabled = true;
            yield return new WaitForSeconds(15f);
          //  PlayerPrefs.SetInt("Help", 1);
            HelpPanel.SetActive(false);
     /*   }
        else
        {
            HelpPanel.SetActive(false);
        }*/
    }

    public void clearsave()
    {
        PlayerPrefs.DeleteKey("Help");
    }
}
